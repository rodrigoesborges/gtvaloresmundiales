---
layout: post 
title: Tiago Camarinha Lopes
---

CV Lattes:[Tiago Camarinha Lopes](http://lattes.cnpq.br/1316863676101106)

Tiago Camarinha Lopes Possui graduação em Ciências Econômicas pela Universidade XXX - Alemanha (2004) e Doutorado em Economia pela Universidade Federal de Uberlândia (UFU). Atualmente é professor adjunto na Universidade Federal de Goiás.
