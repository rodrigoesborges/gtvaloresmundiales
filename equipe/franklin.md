---
layout: post 
title: Rodrigo S. P. Franklin
---

CV Lattes:[Rodrigo Straessli Pinto Franklin](http://lattes.cnpq.br/1316863676101106)

Rodrigo Franklin possui graduação em Ciências Econômicas pela Universidade Federal do Espírito Santo - UFES (2005), mestrado e Doutorado em Economia pela Universidade Federal do Rio Grande do Sul (2014). Atualmente é professor no departamento de Economia da UFES, Vitória. Sua trajetória pesquisadora centra-se na área de Economia Política, Economia Brasileira Contemporânea, Teoria da Dependência, conhecimentos em Insumo-Produto.
