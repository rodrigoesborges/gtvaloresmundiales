---
layout: post 
title: César Sánchez
---

CV Lattes:[Mario César Sánchez](http://lattes.cnpq.br/1316863676101106)

César Sánchez Possui graduação em Ciências Econômicas pela Universidad Nacional Autónoma do México - UNAM(2000), Diploma de Estudos Avançados e Doutorado em Economia pela Universidade Complutense de Madrid (2008). Atualmente é professor da Universidad de Centroamérica - UCA , El Salvador.
