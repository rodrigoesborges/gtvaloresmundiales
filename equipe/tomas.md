---
layout: post 
title: Tomás de Siervi Barcellos
---

CV Lattes:[Tomás de Siervi Barcellos](http://lattes.cnpq.br/1316863676101106)

Tomás Barcellos possui graduação em Ciências Econômicas pela Universidade Federal de Santa Catarina - UFSC (2011) e mestrado em Estudos Latino-americanos pela Universidade de Brasília (UnB). Atualmente é servidor público e atua no Conselho Administrativo de Defesa Econômica (CADE).
