---
layout: post 
title: Everlam Montibeler
---

CV Lattes:[Everlam Elias Montibeler](http://lattes.cnpq.br/1316863676101106)

Everlam Montibeler possui graduação em Ciências Econômicas pela Universidade Federal do Espírito Santo - UFES (2003), Diploma de Estudos Avançados pela Universidade Complutense de Madrid (2004) e Doutorado em Economia pela Universidade Complutense de Madrid (2011). Atualmente é professor no Departamento de Economia da UFES, Vitória.
