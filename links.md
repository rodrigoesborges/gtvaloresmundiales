---
layout: post
title: Nossos colegas e outros vínculos úteis
---

### Links

O grupo de Estudos Concretos sobre Teoria do Valor Trabalho com os seguintes grupos e pesquisadores:

1. [Vitória (BR) - Departamento de Economia - Universidade Federal do Espírito Santo - Rodrigo S. P. Franklin e Everlam Montibeler](https://ufes.br)
2. [El Salvador - Departamento de Economia - Universidad Centroamericana - Cesar Sánchez](https://uca.edu.sv)
3. [Madrid - Instituto Marxista de Economía - Universidad Compluentense de Madrid - Xabier Arrizabalo, Juan Pablo Mateo Tomé](http://ucm.es/)
5. [Departamento de Economia - Universidad de La Habana - Cuba](https://www.uh.cu)


### Colaborações Presentes


### Redes Anteriores
<!--1. [Grupo de Pesquisa sobre Política Social no Chile, Brasil e Cuba](http://politicasocial.ufes.br)-->


### Agências Financiadors
<!--1. [CAPES](http://www.capes.gov.br)-->
<1--2. [FAPES](http://www.fapes.es.gov.br)-->
