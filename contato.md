---
title: Contact
layout: post
---

### E-mail address

Prof. Dr. Rodrigo Straessli Pinto Franklin, <a href="mailto:franklin@worldlabourvalues.org"> franklin@worldlabourvalues.org </a>

### Post address


Universidade Federal do Espírito Santo, Centro de Ciências Jurídicas e Econômicas, Departamento de Serviço Social.
Av. Fernando Ferrari, s/n.
Goiabeiras
29000-000 - Vitoria, ES 
Brasil



### Telefone e Fax
Telefone: (27) 33352587
Fax: (27) 33352587
URL da Homepage: https://worldlabourvalues.org



