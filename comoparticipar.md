---
layout: post
title: Como participar
---
Para participar, tirar dúvidas ou para maiores informações, basta escrever para o e-mail de algum dos participantes, como:

- [Rodrigo Borges](mailto:rodrigo@borges.net.br)

- [Prof. Rodrigo Franklin](mailto:rodrigo.franklin@ufes.br)

