---
layout: post
title: Equipe
---

<!-- <img src = "images/fotogrupo2020.jpg" width="600">


Novembro de  2018. -->

### O êxito na pesquisa é resultado de trabalho em equipe
<img src="{{ site.baseurl }}/images/team.jpg" width="600">



### Professores
* [Rodrigo Straessli Pinto Franklin](equipe/franklin.html), Coordenador - Professor Adjunto - Universidade Federal do Espírito Santo
* [Mario César Sánchez](equipe/cesar.html), Coordenador - Professor Titular - Universidad de Centro América - El Salvador
* [Everlam Montibeler](equipe/ever.html), Coordenador - Professor Adjunto - Universidade Federal do Espírito Santo
* [Tiago Camarinha Lopes](equipe/tiago.html), Professor Adjunto - Universidade Federal de Goiás

### Pesquisadores

* [Rodrigo E. S. Borges, Ph. D.](equipe/rodrigo_borges.html), Coordenador - Doutor em Economia (UCM, Espanha) e Pós-Doutor em Política Social (UFES)
* [Tomás de Siervi Barcellos, Ms. C.](equipe/tomas.html)
* [José Carlos Diaz](equipe/josecarlos.html), Doctorando en la Universidad Nacional Autónoma de México (UNAM)



### Grande Parte do Crédito vai para nossos Colegas de Pesquisa
Veja também [Vínculos e Colegas de Pesquisa](links)




